package uz.demo.auth.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.Collection;

@Getter
@Setter
@Entity
public class Permission extends BaseEntity {

    @Column(nullable = false)
    private String name;

    private String info;

    @ManyToMany(mappedBy = "permissions")
    private Collection<Role> roles;

}
