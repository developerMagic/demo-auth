package uz.demo.auth.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uz.demo.auth.model.dto.UserDto;
import uz.demo.auth.model.entity.Role;
import uz.demo.auth.model.entity.User;
import uz.demo.auth.repository.RoleRepository;
import uz.demo.auth.repository.UserRepository;
import uz.demo.auth.service.UserService;
import uz.demo.auth.utils.SecurityUtils;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("secure/user")
public class UserController {

    private final ObjectMapper objectMapper;
    private final UserService userService;
    private final SecurityUtils securityUtils;
    private final RoleRepository roleRepository;
    private final UserRepository userRepository;

    private final String DEFAULT = "ROLE_USER";

    @Autowired
    public UserController(ObjectMapper objectMapper, UserService userService, SecurityUtils securityUtils, RoleRepository roleRepository, UserRepository userRepository) {
        this.objectMapper = objectMapper;
        this.userService = userService;
        this.securityUtils = securityUtils;
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
    }

    @PreAuthorize("hasAuthority('PERMISSION_USER_INFO')")
    @GetMapping("/info")
    public ObjectNode info() {
        ObjectNode response = objectMapper.createObjectNode();
        User user = securityUtils.getCurrentUser();
        response.putPOJO("user", userService.getUserDto(user));
        response.put("message", "SUCCESS");
        return response;
    }

    @PostMapping("/")
    public ObjectNode save(@RequestParam(value = "first_name", required = false) String firstName,
                           @RequestParam(value = "last_name", required = false) String lastName,
                           @RequestParam(value = "user_name", required = false) String userName,
                           @RequestParam(value = "password", required = false) String password) {
        ObjectNode response = objectMapper.createObjectNode();

        User currentUser = securityUtils.getCurrentUser();

        Optional<User> option = userRepository.findByUserName(userName);
        Optional<Role> optional = roleRepository.findByName(DEFAULT);
        if (!option.isPresent()) {
            User user;
            if (optional.isPresent()) {
                List<Role> roles = Collections.singletonList(optional.get());
                user = new User(firstName, lastName, userName, password, roles);
            } else {
                user = new User(firstName, lastName, userName, password, null);
            }
            User saved = userRepository.save(user);
            response.putPOJO("user", userService.getUserDto(saved));
            response.put("message", "SUCCESS");
        } else {
            User user = option.get();
            if (user.getUserName().equals(currentUser.getUserName())) {
                user.setFirstName(firstName);
                user.setLastName(lastName);
                user.setUserName(userName);
                user.setPassword(password);
                User saved = userRepository.save(user);
                response.putPOJO("user", userService.getUserDto(saved));
                response.put("message", "SUCCESS");
            } else {
                response.putPOJO("user", null);
                response.put("message", "ERROR");
            }
        }
        return response;
    }

    @PreAuthorize("hasAuthority('PERMISSION_USERS_LIST')")
    @PostMapping("/list")
    public ObjectNode list(@RequestParam(value = "page", required = false, defaultValue = "0") int page,
                           @RequestParam(value = "size", required = false, defaultValue = "10") int size,
                           @RequestParam(value = "lastId", required = false, defaultValue = "0") long id) {
        ObjectNode response = objectMapper.createObjectNode();
        List<UserDto> list = userService.findPaginated(page, size, id);
        response.putPOJO("users", list);
        response.put("message", "SUCCESS");
        return response;
    }

    @PreAuthorize("hasAuthority('PERMISSION_USER_DELETE')")
    @PostMapping("/del")
    public ObjectNode delete(@RequestParam(value = "user_name") String username) {
        ObjectNode response = objectMapper.createObjectNode();
        Optional<User> optional = userRepository.findByUserName(username);
        if (optional.isPresent()) {
            userRepository.deleteById(optional.get().getId());
            response.put("message", "SUCCESS");
        } else {
            response.put("message", "ERROR");
        }
        return response;
    }
}
