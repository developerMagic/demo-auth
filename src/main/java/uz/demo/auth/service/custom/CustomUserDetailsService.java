package uz.demo.auth.service.custom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import uz.demo.auth.model.custom.CustomUserDetails;
import uz.demo.auth.model.entity.User;
import uz.demo.auth.repository.UserRepository;

import java.util.Optional;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> optional = userRepository.findByUserName(username);

        optional.orElseThrow(() -> new UsernameNotFoundException("Username not found"));

        return optional.map(CustomUserDetails::new).get();
    }

}
