package uz.demo.auth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import uz.demo.auth.model.dto.UserDto;
import uz.demo.auth.model.entity.User;
import uz.demo.auth.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<UserDto> findPaginated(int page, int size, long lastId) {
        PageRequest pageRequest = PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "id"));
        return getUserDtoList(userRepository.findByIdGreaterThan(lastId, pageRequest));
    }

    public UserDto getUserDto(User user) {
        return new UserDto(user.getFirstName(), user.getLastName(), user.getUserName());
    }

    private List<UserDto> getUserDtoList(List<User> users) {
        List<UserDto> userDtos = new ArrayList<>();
        users.forEach(user -> userDtos.add(getUserDto(user)));
        return userDtos;
    }
}