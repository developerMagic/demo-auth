package uz.demo.auth.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.demo.auth.model.entity.Permission;

@Repository
public interface PermissionRepository extends JpaRepository<Permission, Long> {
}
